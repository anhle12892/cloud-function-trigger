# Trigger Cloud Build from Google Function

This repo contains a sample Cloud Function which can be triggered from GitLab, to use Cloud Build to generate a container from a Dockerfile.

## Requirements

This function and build makes use of a few products:
1. GitLab
1. Cloud Function
1. Google Storage
1. Google KMS
1. Google Container Builder

## Dependencies
1. [gcp-container-builder](https://www.npmjs.com/package/gcp-container-builder)

## Instructions

1. A GitLab project that includes a Dockerfile
1. Set up a [webhook](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html) to trigger the Cloud Function on the desired events (commit, tag, etc)
1. Create a [deploy token](https://docs.gitlab.com/ee/user/project/deploy_tokens/) for the project
1. Create a `.git-credentials` file containing the HTTPS URL of your server, and deploy token. For example `https://username:password@gitlab.com`.
1. Encrypt the `.git-credentials` file with `gcloud kms`. For example: `gcloud kms encrypt --ciphertext-file=.git-credentials.enc --plaintext-file=.git-credentials --location=us-east4 --key=deploy-key --keyring=gitlab-deploy-key`
1. Put the resulting `.git-credentials.enc` file in a GCS storage bucket
1. Grant the GCB service account the `Storage Object Viewer` and `Cloud KMS CryptoKey Decrypter` roles.
1. Update `index.js` for the created key & keyring.
1. Save `index.js` and `package.json` to a Cloud Function

## Opportunities for enhancements

1. Parse the incoming webhook for what branch the change was on, to git pull the proper branch. Right now it will always pull the default branch.